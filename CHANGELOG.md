# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.3.0 - 2023-10-03(06:58:16 +0000)

### New

- [amx][conmon] ConMon must support arp(ipv4) and icmpv6(ipv6)

## Release v1.2.0 - 2023-04-04(15:35:19 +0000)

### New

- Create an event when a renew is received

## Release v1.1.0 - 2023-03-13(15:57:01 +0000)

### Fixes

- Issue : HOP-2088 [DHCPv6] Split client and server plugin

## Release v1.0.1 - 2022-04-14(07:28:43 +0000)

### Changes

- Make the Interface path prefixed

## Release v1.0.0 - 2022-02-28(10:56:57 +0000)

### Breaking

- Use amxm to start/stop syncing netmodel clients

## Release v0.2.2 - 2022-01-27(12:47:43 +0000)

### Fixes

- wrong mib can be unsubscribed when netmodel clients share a netmodel interface

## Release v0.2.1 - 2022-01-21(16:26:00 +0000)

### Changes

- expand libnetmodel for mibs/netmodel-clients

## Release v0.2.0 - 2022-01-11(16:51:49 +0000)

### New

- Create DHCPv6 Client mapping in NetModel

## Release v0.1.0 - 2022-01-11(14:23:24 +0000)

### New

- Create DHCPv6 Client mapping in NetModel

