# netmodel-dhcp6

This netmodel client handles the synchronization between netmodel interfaces marked with the dhcpv6 flag and the DHCPv6.Client.Intf. instances.